# CentraleSupélec Coding Weeks : Application Android

Ces coding weeks sont divisées en deux séquences :
- Au cours de la première semaine vous allez découvrir les bases du développement d'applications Android. Le premier jour sera consacré à une introduction au langage Java puis vous développerez par incréments une application de Trivial Pursuit.
- Au cours de la deuxième semaine vous aurez pour projet en groupe de 4 ou 5 étudiants d'imaginer une nouvelle application Android réutilisant les éléments découverts en première semaine et de la développer.

Informations utiles :
- Tous les développements se feront en Java. Il n'est pas nécessaire de connaître déjà ce langage pour choisir ce sujet mais il faut être conscient que se familiariser avec un nouveau language, des concepts de programmation non couverts dans le cours de SIP et les mettre en oeuvre en moins de 4 jours peut représenter un challenge pour les étudiants les moins à l'aise en programmation. 
- Il n'est pas nécessaire de posséder un téléphone sous Android pour tester vos développements.
- La première semaine se déroule sur le campus de Metz. Le déplacement en train (départ à 8:13 de la gare de l'Est le lundi, retour à la gare de l'Est à 21:14 le jeudi 10) et l'hébergement à l'hôtel sont pris en charge par l'école pour les étudiants du campus de Saclay. La deuxième semaine se déroule sur votre campus habituel.
- La première semaine chaque groupe de 24 étudiants sera encadré par un professeur en permanence. Cette semaine sera évaluée individuellement sur la base de quizzes de contrôle continu et de l'avancement de votre application de trivial pursuit. La deuxième semaine vous travaillerez en autonomie. Le travail de cette semaine sera évalué sur la base d'une soutenance de groupe le vendredi après-midi et de la qualité du codé livré.
- Vous n'avez pas de logiciel à installer car vous pourrez travailler sur les PC de l'école, sur place pour la première semaine, puis à distance la seconde. Pour ceux qui préfèrent néanmoins utiliser leur portable, quel que soit votre système, il vous faut installer [Android Studio](https://developer.android.com/studio).


